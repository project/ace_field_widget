(function ($) {

  Drupal.behaviors.aceFieldWidget = {
    attach: function (context, settings) {

      $('textarea[data-editor]').each(function () {
        var textarea = $(this);
        var editor_config = Drupal.settings.ace_field_widget.fields[this.id];
        console.log(editor_config);
        var mode = textarea.data('editor');

        var editDiv = $('<div>', {
          position: 'absolute',
          width: textarea.width(),
          height: textarea.height(),
          'class': textarea.attr('class')
        }).insertBefore(textarea);

        textarea.css('display', 'none');

        ace.config.set('basePath', '/sites/all/libraries/ace/src');
        var langTools = ace.require("ace/ext/language_tools");
        var editor = ace.edit(editDiv[0]);
        editor.getSession().setValue(textarea.val());
        editor.getSession().setMode("ace/mode/" + mode);
        editor.getSession().setShowLineNumbers;
        editor.setTheme("ace/theme/" + editor_config.theme);

        // copy back to textarea on form submit...
        textarea.closest('form').submit(function () {
          textarea.val(editor.getSession().getValue());
        })

      });

    }
  };

})(jQuery);
